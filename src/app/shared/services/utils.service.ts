import { Injectable, Inject } from '@angular/core';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
            @Inject(PLATFORM_ID) private platformId: any,
            @Inject('WINDOW') private window: any) { }

  getWindowOffset() {
    if (isPlatformBrowser(this.platformId)) {
      return this.window.pageYOffset;
    } else {
      return 0;
    }
  }

  scrollTop() {
    if (isPlatformBrowser(this.platformId)) {
     this.window.scrollTo(0, 0);
    }
  }
}


