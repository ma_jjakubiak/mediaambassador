export interface Breakpoints {
  desktop: string;
  laptop: string;
  laptopSmall: string;
  tablet: string;
  tabletSmall: string;
  phone: string;
  phoneSmall: string;
}
