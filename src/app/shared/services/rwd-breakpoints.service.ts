import { Injectable } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Breakpoints } from './services.model';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class RwdBreakpointsService {
  breakpoints: Breakpoints = {
    desktop: '(max-width: 1520px)',
    laptop: '(max-width: 1366px)',
    laptopSmall: '(max-width: 1200px)',
    tablet: '(max-width: 1024px)',
    tabletSmall: '(max-width: 768px)',
    phone: '(max-width: 500px)',
    phoneSmall: '(max-width: 320px)'
  };

  constructor(private breakpointsObserver: BreakpointObserver) {}

  getDesktopState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.desktop}`]);
  }

  getLaptopState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.laptop}`]);
  }

  getLaptopSmallState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.laptopSmall}`]);
  }

  getTabletState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.tablet}`]);
  }

  getTabletSmallState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.tabletSmall}`]);
  }

  getPhoneState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.phone}`]);
  }

  getPhoneSmallState(): Observable<BreakpointState> {
    return this.breakpointsObserver.observe([`${this.breakpoints.phoneSmall}`]);
  }
}
