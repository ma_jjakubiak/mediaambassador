import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

export const SlideLeft = trigger('SlideLeft', [
  transition(':enter', [
    style({ transform: 'translateX(100%)'}),
    animate('300ms ease-in-out', style({transform: 'translateX(0)'}))
  ]),
  transition(':leave', [
    style({ transform: 'translateX(0)'}),
    animate('300ms ease-in-out', style({transform: 'translateX(100%)'}))
  ])
]);

export const MenuVisibility = trigger('slideMenu', [
  state('static', style({
    position: 'relative',
    transform: 'translateY(0)'
  })),
  state('show', style({
    position: 'fixed',
    top: 0,
    left: 0,
    transform: 'translateY(0)'
  })),
  state('hide', style({
    position: 'fixed',
    top: 0,
    left: 0,
    transform: 'translateY(-100px)'
  })),
  transition('show <=> hide', animate('350ms ease-in-out')),
  transition('show => static', animate('200ms ease-in-out', style({ transform: 'translateY(-100px)' })))
]);

export const RouterAnimations = trigger('routeAnimations', [
  transition('* => home', [
    query(':enter', [
      style({
        opacity: '0.3',
        willChange: 'opacity',
      }),
      animate('500ms ease-in-out', style({ opacity: '1'}))
    ], { optional: true })
  ]),
  transition('* <=> *', [
    query(':enter', [
      style({
        opacity: '0.3',
        willChange: 'transform',
        transform: 'scale(0.98)'
      }),
      stagger(150, [
        animate('400ms ease-in-out', style({ opacity: '1', transform: 'scale(1)' }))
      ])
      ], { optional: true })
  ]),
]);
