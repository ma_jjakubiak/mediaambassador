import { Component, OnInit } from '@angular/core';
import { careerJobs } from '../../../mock/careerJobs';

@Component({
  selector: 'ma-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.scss']
})
export class CareerComponent implements OnInit {
  careers = careerJobs;

  constructor() { }

  ngOnInit() {
  }

}
