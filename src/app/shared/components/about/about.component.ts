import { Component, OnInit } from '@angular/core';
import { aboutText } from '../../../mock/aboutText';

@Component({
  selector: 'ma-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  aboutText: string[] = aboutText;

  constructor() { }

  ngOnInit() {
  }

}
