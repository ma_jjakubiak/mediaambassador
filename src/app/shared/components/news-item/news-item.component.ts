import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ma-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.scss']
})
export class NewsItemComponent implements OnInit {
  @Input() path = '1';

  constructor() { }

  ngOnInit() {
  }

}
