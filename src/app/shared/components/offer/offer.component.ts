import { Component, OnInit } from '@angular/core';
import { offerIcons, offerInfo } from '../../../mock/offerIcons';
import { RwdBreakpointsService } from '../../../shared/services/rwd-breakpoints.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'ma-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {
  isSmallDetail = false;
  isBigDetail = false;
  offersIcons = offerIcons.filter((offer, i) =>  i < offerIcons.length - 1 );
  details: any;
  offers = offerIcons;
  isTablet = false;

  subscription = new Subscription;

  constructor(private breakpointsService: RwdBreakpointsService) { }

  ngOnInit() {
    this.subscription.add(this.breakpointsService.getTabletState().subscribe(state => {
      this.isTablet = state.matches;
    }));
  }

  showDetails(textNumber: number) {
    if (textNumber === 9) {
      this.isSmallDetail = true;
    } else {
      this.isBigDetail = true;
    }

    this.details = offerInfo[textNumber];
  }

  hideDetails() {
    this.isSmallDetail = false;
    this.isBigDetail = false;
  }

}
