import { Component, Renderer2, ContentChildren, AfterContentInit } from '@angular/core';

@Component({
  selector: 'ma-simple-swiper',
  templateUrl: './simple-swiper.component.html',
  styleUrls: ['./simple-swiper.component.scss']
})
export class SimpleSwiperComponent implements AfterContentInit {
  sliderLength = 0;
  currentSlide = 1;
  sliderTransform = 0;

  @ContentChildren('sliderItem') sliderItems: any;

  constructor(private renderer: Renderer2) { }

  ngAfterContentInit() {
    setTimeout(() => { this.sliderLength = this.sliderItems.length; });
  }

  moveSliderElements(position) {
    this.sliderItems.forEach(slider => {
      this.renderer.setStyle(slider.nativeElement, 'transform', `translateX(${position}%)`);
    });
  }

  previousSlide() {
    if (this.currentSlide > 1) {
      this.currentSlide = this.currentSlide - 1;
      this.sliderTransform = (this.currentSlide - 1) * -100;

      this.moveSliderElements(this.sliderTransform);
    }
  }

  nextSlide() {
    if (this.currentSlide < this.sliderLength) {
      this.currentSlide = this.currentSlide + 1;
      this.sliderTransform = (this.currentSlide - 1) * -100;

      this.moveSliderElements(this.sliderTransform);
    }
  }
}
