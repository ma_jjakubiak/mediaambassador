import { Component, OnInit } from '@angular/core';
import { portfolioMock } from '../../../mock/porftolioMock';

@Component({
  selector: 'ma-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  portfolio = portfolioMock;
  constructor() { }

  ngOnInit() {
  }

}
