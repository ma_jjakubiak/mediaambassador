import { Component, OnInit } from '@angular/core';
import { starCommerceTexts, maxCommerceTexts, fideloTexts } from '../../../mock/solutionsTexts';

@Component({
  selector: 'ma-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.scss']
})
export class SolutionsComponent implements OnInit {
  solutions = starCommerceTexts;
  maxSolutions = maxCommerceTexts;
  fidelo = fideloTexts;

  constructor() { }

  ngOnInit() {
  }

}
