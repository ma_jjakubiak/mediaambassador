import { Component, OnInit } from '@angular/core';
import { contactMock } from '../../../mock/contactMock';
import { MapTypeStyle } from '@agm/core';

@Component({
  selector: 'ma-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contacts: string[] = contactMock;
  lat = 51.071766;
  lng = 17.006698;
  zoom = 16;

  mapStyles: MapTypeStyle[] = [
    {
      'featureType': 'administrative.province',
      'elementType': 'all',
      'stylers': [
          {'visibility': 'off'}
      ]
    },
    {
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [
            {'saturation': -100},
            {'lightness': 65},
            {'visibility': 'on'}
        ]
    },
    {
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': [
            {'saturation': -100},
            {'lightness': 51},
            {'visibility': 'simplified'}
        ]
    },
    {
        'featureType': 'road.highway',
        'elementType': 'all',
        'stylers': [
            {'saturation': -100},
            {'visibility': 'simplified'}
        ]
    },
    {
        'featureType': 'road.arterial',
        'elementType': 'all',
        'stylers': [
            {'saturation': -100},
            {'lightness': 30},
            {'visibility': 'on'}
        ]
    },
    {
        'featureType': 'road.local',
        'elementType': 'all',
        'stylers': [
            {'saturation': -100},
            {'lightness': 40},
            {'visibility': 'on'}
        ]
    },
    {
        'featureType': 'transit',
        'elementType': 'all',
        'stylers': [
            {'saturation': -100},
            {'visibility': 'simplified'}
        ]
    },
    {
        'featureType': 'transit',
        'elementType': 'geometry.fill',
        'stylers': [
            {'visibility': 'on'}
        ]
    },
    {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [
            {'hue': '#ffff00'},
            {'lightness': -25},
            {'saturation': -97}
        ]
    },
    {
        'featureType': 'water',
        'elementType': 'labels',
        'stylers': [
            {'visibility': 'on'},
            {'lightness': -25},
            {'saturation': -100}
        ]
    }
  ];


  constructor() { }

  ngOnInit() {
  }

}
