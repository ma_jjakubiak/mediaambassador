import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ma-client-box',
  templateUrl: './client-box.component.html',
  styleUrls: ['./client-box.component.scss']
})
export class ClientBoxComponent implements OnInit {
  @Input() path = 'bigstar';

  constructor() { }

  ngOnInit() {
  }

}
