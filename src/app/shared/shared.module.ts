import { NgModule } from '@angular/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { SimpleSwiperComponent } from './components/simple-swiper/simple-swiper.component';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { ScrollToModule } from 'ng2-scroll-to-el';
import { AboutComponent } from './components/about/about.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { OfferComponent } from './components/offer/offer.component';
import { SolutionsComponent } from './components/solutions/solutions.component';
import { CareerComponent } from './components/career/career.component';
import { ContactComponent } from './components/contact/contact.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../environments/environment.prod';
import { FilterBeltComponent } from './components/filter-belt/filter-belt.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { RouterModule } from '@angular/router';
import { ClientBoxComponent } from './components/client-box/client-box.component';
import { ItemsViewBoxComponent } from './components/items-view-box/items-view-box.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    InlineSVGModule,
    ScrollToModule,
    TranslateModule.forChild(),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapKey
    })
  ],
  exports: [
    InlineSVGModule,
    SimpleSwiperComponent,
    TranslateModule,
    ScrollToModule,
    AboutComponent,
    PortfolioComponent,
    OfferComponent,
    SolutionsComponent,
    CareerComponent,
    ContactComponent,
    FilterBeltComponent,
    NewsItemComponent,
    ClientBoxComponent,
    ItemsViewBoxComponent
  ],
  declarations: [
    SimpleSwiperComponent,
    AboutComponent,
    PortfolioComponent,
    OfferComponent,
    SolutionsComponent,
    CareerComponent,
    ContactComponent,
    FilterBeltComponent,
    NewsItemComponent,
    ClientBoxComponent,
    ItemsViewBoxComponent]
})
export class SharedModule {}
