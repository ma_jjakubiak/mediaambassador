import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaseLayout } from './views/base/base.layout';
import { BaseModule } from './views/base/base.layout.module';

const routes: Routes = [
  { path: '', component: BaseLayout,
    children: [
      { path: '', loadChildren: './views/home/home.module#HomeModule'},
      { path: 'about', loadChildren: './views/about/about.module#AboutModule' },
      { path: 'career', loadChildren: './views/career/career.module#CareerModule' },
      { path: 'clients', loadChildren: './views/clients/clients.module#ClientsModule' },
      { path: 'contact', loadChildren: './views/contact/contact.module#ContactModule' },
      { path: 'news', loadChildren: './views/news/news.module#NewsModule' },
      { path: 'offer', loadChildren: './views/offer/offer.module#OfferModule' },
      { path: 'solutions', loadChildren: './views/solutions/solutions.module#SolutionsModule' },
      { path: 'news/:id', loadChildren: './views/news-item/news-item.module#NewsItemModule' },
      { path: 'clients/:id', loadChildren: './views/clients-item/clients-item.module#ClientsItemModule' }
    ]
  }

];

@NgModule({
  imports: [
    BaseModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
