import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfferView } from './offer.view';

const routes: Routes = [
  {
    path: '',
    component: OfferView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class OfferRoutingModule { }
