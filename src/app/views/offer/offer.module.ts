import { NgModule } from '@angular/core';
import { OfferView } from './offer.view';
import { OfferRoutingModule } from './offer-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ OfferRoutingModule, SharedModule ],
  declarations: [ OfferView ]
})

export class OfferModule { }
