import { Component, OnInit, OnDestroy, HostListener, Input } from '@angular/core';
import { RwdBreakpointsService } from '../../../shared/services/rwd-breakpoints.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SlideLeft } from '../../../shared/animations';
import { UtilsService } from '../../../shared/services/utils.service';
import { MenuVisibility } from '../../../shared/animations';


@Component({
  selector: 'ma-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
  animations: [ SlideLeft, MenuVisibility ]
})
export class MainNavComponent implements OnInit, OnDestroy {
  _isScrollTop = false;
  isTablet = false;
  isMobileNavOpen = false;
  scrollPosition = -1;
  menuState = 'static';
  staticOffset = 300;
  subscription = new Subscription;

  @Input('isScrollTop') set isScrollTop(scroll: boolean) {
    this._isScrollTop = scroll;
  }

  @HostListener('window:wheel', ['$event'])
  @HostListener('touchmove', ['$event'])
  @HostListener('window:scroll', ['$event']) onWindowScroll(event) {
    if (this.isTablet && this.isMobileNavOpen) {
      event.preventDefault();
      return;
    }

    if (this._isScrollTop) {
      this.menuState = 'static';
    } else {
      if (this.utils.getWindowOffset() < this.staticOffset) {
        this.menuState = 'static';
      } else if (this.scrollPosition !== this.utils.getWindowOffset()) {
        this.menuState = this.scrollPosition < this.utils.getWindowOffset() ? 'hide' : 'show';
      }
      this.scrollPosition = this.utils.getWindowOffset();
    }
  }


  constructor(private breakpointsService: RwdBreakpointsService,
              private utils: UtilsService) {}

  ngOnInit() {
    this.subscription.add(this.breakpointsService.getTabletState().subscribe(tablet => {
      this.isTablet = tablet.matches;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toggleMobileMenu() {
    this.isMobileNavOpen = !this.isMobileNavOpen;
  }

}
