import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'ma-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  breadcrumbs: string[] = [];
  currentBreadcrumb = '';
  currentTitle = '';

  constructor(private router: Router) { }

  ngOnInit() {
    this.breadcrumbs = this.setBreadcrumbs(this.router.url);
    this.currentTitle = this.setCurrentTitle(this.router.url);
    this.currentBreadcrumb = this.router.url;

    this.router.events.subscribe(val => {
      if (!!val && val instanceof NavigationStart) {
        this.currentTitle = null;
      }

      if (!!val && val instanceof NavigationEnd) {
        setTimeout(() => {
          this.breadcrumbs = this.setBreadcrumbs(val.url);
          this.currentTitle = this.setCurrentTitle(val.url);
          this.currentBreadcrumb = val.url;
        }, 0);
      }
    });


  }

  setBreadcrumbs(url) {
    const path = url.split('/');
    return path.filter( (crumb, i) => i > 0 && i !== path.length - 1 );

  }

  setCurrentTitle(url) {
    const path = url.split('/');
    return path[path.length - 1];

  }

}
