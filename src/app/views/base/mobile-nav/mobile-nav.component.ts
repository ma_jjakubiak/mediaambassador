import { Component, OnInit, EventEmitter, Output, HostListener, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'ma-mobile-nav',
  templateUrl: './mobile-nav.component.html',
  styleUrls: ['./mobile-nav.component.scss']
})
export class MobileNavComponent implements OnInit, OnDestroy {
  @Output() close = new EventEmitter();

  subscription = new Subscription;

  @HostListener('click') closeNav() {
    this.onClose();
  }

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.subscription.add(this.router.events.subscribe(val => {
      if (!!val && val instanceof NavigationEnd) {
        this.onClose();
      }
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onClose() {
    this.close.emit(null);
  }

}
