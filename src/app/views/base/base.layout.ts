import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ScrollToService } from 'ng2-scroll-to-el';
import { RwdBreakpointsService } from '../../shared/services/rwd-breakpoints.service';

@Component({
  selector: 'ma-base',
  templateUrl: './base.layout.html',
  styleUrls: ['./base.layout.scss']
})
export class BaseLayout implements OnInit, OnDestroy {
  subscription = new Subscription;
  isScrollTop = false;
  isHome = false;
  isTabletSmall = false;
  currentUrl: string;

  constructor(private router: Router,
              private scrollService: ScrollToService,
              private rwdBreakpointsService: RwdBreakpointsService) { }

  ngOnInit() {
    this.isHome = this.router.url === '/';
    this.currentUrl = this.router.url;

    this.subscription.add(this.router.events.subscribe(val => {
      if (!!val && val instanceof NavigationStart) {
        this.scrollTop('#top', 300);

      } else if (!!val && val instanceof NavigationEnd) {
        this.isHome = this.router.url === '/';
        this.currentUrl = this.router.url;
      }
    }));

    this.subscription.add(this.rwdBreakpointsService.getTabletSmallState().subscribe(state => {
      this.isTabletSmall = state.matches;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  scrollTop(element: string, duration: number = 300) {
    this.isScrollTop = true;

    this.scrollService.scrollTo(element, duration);
    setTimeout(() => { this.isScrollTop = false; }, 400);
  }

}
