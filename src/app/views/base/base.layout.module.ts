import { NgModule } from '@angular/core';
import { MainNavComponent } from './main-nav/main-nav.component';
import { RouterModule } from '@angular/router';
import { BaseLayout } from './base.layout';
import { CommonModule } from '@angular/common';
import { MobileNavComponent } from './mobile-nav/mobile-nav.component';
import { SharedModule } from '../../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../../environments/environment.prod';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';


@NgModule({
  imports: [ RouterModule,
    CommonModule,
    SharedModule,
    AgmCoreModule.forRoot({
    apiKey: environment.googleMapKey
  }) ],
  declarations: [ MainNavComponent, BaseLayout, MobileNavComponent, BreadcrumbsComponent ],
  exports: [ BaseLayout ]
})
export class BaseModule {}
