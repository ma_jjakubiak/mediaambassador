import { NgModule } from '@angular/core';
import { NewsItemView } from './news-item.view';
import { NewsItemRoutingModule } from './news-item-routing.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ SharedModule, CommonModule, NewsItemRoutingModule ],
  declarations: [ NewsItemView ]
})

export class NewsItemModule { }
