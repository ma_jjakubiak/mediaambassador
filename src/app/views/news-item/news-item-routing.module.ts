import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsItemView } from './news-item.view';

const routes: Routes = [
  {
    path: '',
    component: NewsItemView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class NewsItemRoutingModule { }
