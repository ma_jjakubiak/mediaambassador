import { NgModule } from '@angular/core';
import { SolutionsView } from './solutions.view';
import { SolutionsRoutingModule } from './offer-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ SolutionsRoutingModule, SharedModule ],
  declarations: [ SolutionsView ]
})

export class SolutionsModule { }
