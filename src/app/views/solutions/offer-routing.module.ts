import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolutionsView } from './solutions.view';

const routes: Routes = [
  {
    path: '',
    component: SolutionsView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class SolutionsRoutingModule { }
