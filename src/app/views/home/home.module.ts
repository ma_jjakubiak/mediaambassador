import { NgModule } from '@angular/core';
import { MainBannerComponent } from './main-banner/main-banner.component';
import { HomeView } from './home.view';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [ HomeRoutingModule,
    SharedModule,
    CommonModule
  ],
  declarations: [ MainBannerComponent,
                  HomeView ]
})

export class HomeModule { }
