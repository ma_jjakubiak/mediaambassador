import { Component, OnInit } from '@angular/core';
import { bannerText } from '../../../mock/bannerText';

@Component({
  selector: 'ma-main-banner',
  templateUrl: './main-banner.component.html',
  styleUrls: ['./main-banner.component.scss']
})
export class MainBannerComponent implements OnInit {
  bannerText = bannerText;

  constructor() { }

  ngOnInit() {
  }
}
