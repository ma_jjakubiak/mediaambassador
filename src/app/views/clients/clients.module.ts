import { NgModule } from '@angular/core';
import { ClientsView } from './clients.view';
import { ClientsRoutingModule } from './clients-routing.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ ClientsRoutingModule, CommonModule, SharedModule ],
  declarations: [ ClientsView ]
})

export class ClientsModule { }
