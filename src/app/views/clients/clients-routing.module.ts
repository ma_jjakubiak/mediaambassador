import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsView } from './clients.view';

const routes: Routes = [
  {
    path: '',
    component: ClientsView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class ClientsRoutingModule { }
