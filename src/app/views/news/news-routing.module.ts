import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsView } from './news.view';

const routes: Routes = [
  {
    path: '',
    component: NewsView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class NewsRoutingModule { }
