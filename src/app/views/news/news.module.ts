import { NgModule } from '@angular/core';
import { NewsView } from './news.view';
import { NewsRoutingModule } from './news-routing.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ SharedModule, CommonModule, NewsRoutingModule ],
  declarations: [ NewsView ]
})

export class NewsModule { }
