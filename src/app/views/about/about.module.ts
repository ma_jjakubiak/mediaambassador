import { NgModule } from '@angular/core';
import { AboutView } from './about.view';
import { AboutRoutingModule } from './about-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ AboutRoutingModule, SharedModule ],
  declarations: [ AboutView ]
})

export class AboutModule { }
