import { NgModule } from '@angular/core';
import { ContactView } from './contact.view';
import { ContactRoutingModule } from './contact-routing.module';

@NgModule({
  imports: [ ContactRoutingModule ],
  declarations: [ ContactView ]
})

export class ContactModule { }
