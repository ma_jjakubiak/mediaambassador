import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CareerView } from './career.view';

const routes: Routes = [
  {
    path: '',
    component: CareerView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class CareerRoutingModule { }
