import { NgModule } from '@angular/core';
import { CareerView } from './career.view';
import { CareerRoutingModule } from './career-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ CareerRoutingModule, SharedModule ],
  declarations: [ CareerView ]
})

export class CareerModule { }
