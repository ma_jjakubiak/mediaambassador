import { NgModule } from '@angular/core';
import { ClientsItemView } from './clients-item.view';
import { ClientsItemRoutingModule } from './clients-item-routing.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ SharedModule, CommonModule, ClientsItemRoutingModule ],
  declarations: [ ClientsItemView ]
})

export class ClientsItemModule { }
