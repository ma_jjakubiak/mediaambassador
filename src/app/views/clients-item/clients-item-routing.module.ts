import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsItemView } from './clients-item.view';

const routes: Routes = [
  {
    path: '',
    component: ClientsItemView,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class ClientsItemRoutingModule { }
