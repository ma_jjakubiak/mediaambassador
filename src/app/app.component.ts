import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { environment } from './../environments/environment';
import { RouterOutlet, Router } from '@angular/router';
import { RouterAnimations } from './shared/animations';
import { RwdBreakpointsService } from './shared/services/rwd-breakpoints.service';

@Component({
  selector: 'ma-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ RouterAnimations ]
})
export class AppComponent implements OnInit {
  title = 'mediaambassador';
  gtmTrackUrl: SafeResourceUrl;
  isTablet = false;

  constructor(private domSanitizer: DomSanitizer,
              private router: Router,
              private rwdBreakpointsService: RwdBreakpointsService) {}

  ngOnInit() {
    this.rwdBreakpointsService.getTabletState().subscribe(state => {
      this.isTablet = state.matches;
    });
    this.gtmTrackUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(`https://www.googletagmanager.com/ns.html?id=${environment.gtmTrackId}`);
  }

  getPage(outlet: RouterOutlet) {
    if (!!outlet && outlet.isActivated && !this.isTablet) {
      if (this.router.url === '/') {
        return 'home';
      } else {
        return this.router.url;
      }
    }

    return null;
  }
}
