export const starCommerceTexts = [
  ['solutions.starcommerce.text1', 'solutions.starcommerce.text2', 'solutions.starcommerce.text3', 'solutions.starcommerce.text4', 'solutions.starcommerce.text5'],
  ['solutions.starcommerce.text6', 'solutions.starcommerce.text7', 'solutions.starcommerce.text8']
];

export const maxCommerceTexts = [
  'solutions.maxcommerce.text1',
  'solutions.maxcommerce.text2',
  'solutions.maxcommerce.text3',
  'solutions.maxcommerce.text4',
  'solutions.maxcommerce.text5',
  'solutions.maxcommerce.text6',
  'solutions.maxcommerce.text7'
];

export const fideloTexts = [
  'solutions.fidelo.howItWorks.1',
  'solutions.fidelo.howItWorks.2',
  'solutions.fidelo.howItWorks.3'
];

