export const offerIcons = [
  {
    svg: '/assets/offer/ico-01.svg',
    text: 'offer.focusGroup',
    longText: 'offer.focusGroup.long'
  },
  {
    svg: '/assets/offer/ico-02.svg',
    text: 'offer.analysis',
    longText: 'offer.analysis.long'
  },
  {
    svg: '/assets/offer/ico-03.svg',
    text: 'offer.strategy',
    longText: 'offer.strategy.long'
  },
  {
    svg: '/assets/offer/ico-04.svg',
    text: 'offer.processes',
    longText: 'offer.processes.long'
  },
  {
    svg: '/assets/offer/ico-05.svg',
    text: 'offer.project',
    longText: 'offer.project.long'
  },
  {
    svg: '/assets/offer/ico-06.svg',
    text: 'offer.communicationDesign',
    longText: 'offer.communicationDesign.long'
  },
  {
    svg: '/assets/offer/ico-07.svg',
    text: 'offer.implementation',
    longText: 'offer.implementation.long'
  },
  {
    svg: '/assets/offer/ico-08.svg',
    text: 'offer.marketing',
    longText: 'offer.marketing.long'
  },
  {
    svg: '/assets/offer/ico-09.svg',
    text: 'offer.crm',
    longText: 'offer.crm.long'
  },
  {
    svg: '/assets/offer/ico-10.svg',
    text: 'offer.lean',
    longText: 'offer.lean.long'
  }
];

export const offerInfo = [
  ['offer.focusGroup', 'offer.focusGrop.info1', 'offer.focuGroup.info2', 'offer.focusGroup.info3'],
  ['offer.analysis', 'offer.analysis.info1', 'offer.analysis.info2'],
  ['offer.strategy', 'offer.strategy.info1', 'offer.strategy.info2', 'offer.strategy.info3'],
  ['offer.processes', 'offer.processes.info1', 'offer.processes.info2'],
  ['offer.project', 'offer.project.info1', 'offer.project.info2', 'offer.project.info3'],
  ['offer.communicationDesign', 'offer.communicationDesign.info1', 'offer.communicationDesign.info2', 'offer.communicationDesign.info3'],
  ['offer.implementation', 'offer.implementation.info1', 'offer.implementation.info2', 'offer.implementation.info3'],
  ['offer.marketing', 'offer.marketing.info1', 'offer.marketing.info2', 'offer.marketing.info3', 'offer.marketing.info4', 'offer.marketing.info4'],
  ['offer.crm', 'offer.crm.info1'],
  ['offer.lean', 'offer.lean.info1', 'offer.lean.info2']
];

