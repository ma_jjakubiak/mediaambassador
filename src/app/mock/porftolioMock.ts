export const portfolioMock = [
  {
    'path': '/assets/clients/1.png',
    'alt': 'answer'
  },
  {
    'path': '/assets/clients/2.png',
    'alt': 'bayer'
  },
  {
    'path': '/assets/clients/3.png',
    'alt': 'big star'
  },
  {
    'path': '/assets/clients/4.png',
    'alt': 'but sklep pl'
  },
  {
    'path': '/assets/clients/5.png',
    'alt': 'bytom'
  },
  {
    'path': '/assets/clients/6.png',
    'alt': 'ccc'
  },
  {
    'path': '/assets/clients/7.png',
    'alt': 'coccodrillo'
  },
  {
    'path': '/assets/clients/8.png',
    'alt': 'cropp'
  },
  {
    'path': '/assets/clients/9.png',
    'alt': 'dr irena eris'
  },
  {
    'path': '/assets/clients/10.png',
    'alt': 'empik'
  },
  {
    'path': '/assets/clients/11.png',
    'alt': 'endo.'
  },
  {
    'path': '/assets/clients/12.png',
    'alt': 'gazeta.pl'
  },
  {
    'path': '/assets/clients/13.png',
    'alt': 'gino rossi'
  },
  {
    'path': '/assets/clients/14.png',
    'alt': 'grupa żywiec'
  },
  {
    'path': '/assets/clients/15.png',
    'alt': 'hilding anders'
  },
  {
    'path': '/assets/clients/16.png',
    'alt': 'house'
  },
  {
    'path': '/assets/clients/17.png',
    'alt': 'ibm'
  },
  {
    'path': '/assets/clients/18.png',
    'alt': 'indykpol'
  },
  {
    'path': '/assets/clients/19.png',
    'alt': 'intermarche'
  },
  {
    'path': '/assets/clients/20.png',
    'alt': 'ks sport'
  },
  {
    'path': '/assets/clients/21.png',
    'alt': 'lpp'
  },
  {
    'path': '/assets/clients/22.png',
    'alt': 'marylin'
  },
  {
    'path': '/assets/clients/23.png',
    'alt': 'medicine'
  },
  {
    'path': '/assets/clients/24.png',
    'alt': 'mlekolandia.pl'
  },
  {
    'path': '/assets/clients/25.png',
    'alt': 'muszkieterowie'
  },
  {
    'path': '/assets/clients/26.png',
    'alt': 'nestle'
  },
  {
    'path': '/assets/clients/27.png',
    'alt': 'onet'
  },
  {
    'path': '/assets/clients/28.png',
    'alt': 'Pentel'
  },
  {
    'path': '/assets/clients/29.png',
    'alt': 'polpharma'
  },
  {
    'path': '/assets/clients/30.png',
    'alt': 'pwc'
  },
  {
    'path': '/assets/clients/31.png',
    'alt': 'qubus hotel'
  },
  {
    'path': '/assets/clients/32.png',
    'alt': 'redan'
  },
  {
    'path': '/assets/clients/33.png',
    'alt': 'reporter'
  },
  {
    'path': '/assets/clients/34.png',
    'alt': 'scanholiday'
  },
  {
    'path': '/assets/clients/35.png',
    'alt': 'silesia jeans'
  },
  {
    'path': '/assets/clients/36.png',
    'alt': 'skalnik.pl'
  },
  {
    'path': '/assets/clients/37.png',
    'alt': 'surge'
  },
  {
    'path': '/assets/clients/38.png',
    'alt': 'tołpa'
  },
  {
    'path': '/assets/clients/39.png',
    'alt': 'top secret'
  },
  {
    'path': '/assets/clients/40.png',
    'alt': 'troll'
  },
  {
    'path': '/assets/clients/41.png',
    'alt': 'tui'
  },
  {
    'path': '/assets/clients/42.png',
    'alt': 'urban city'
  },
  {
    'path': '/assets/clients/43.png',
    'alt': 'wojas'
  },
  {
    'path': '/assets/clients/44.png',
    'alt': 'zanox'
  }
];



